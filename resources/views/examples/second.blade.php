{{--

    Example page #2.
    Form tracking

    Installation of uxtester is quick and easy - 
    just copy tracking code from your admin panel 
    and paste right before the end of your site. 

    Important information:
    FormTracker does NOT store / send any data
    inserted by users. It only stores information
    about time of execution.

--}}
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Example page #2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <style>

    /* Just a basic styling for readability. */

    body {
        background: #efefef;
        font-family: 'Open Sans';
    }
    section {
        background: #fff;
        border: 1px solid #dbdbdb;
        border-radius: 4px;
        max-width: 700px;
        margin: 30px auto;
        padding: 25px;
    }
    form {
        border-top: 1px solid #dbdbdb;
        margin-top: 20px;
        padding-top: 20px;
    }
    label {
        font-weight: 600;
    }
    input[type='text'],
    input[type='password'],
    select {
        font-family: 'Open Sans';
        padding: 10px;
        width: 300px;
    }
    textarea {
        font-family: 'Open Sans';
        padding: 10px;
        width: 300px;
    }
    </style>
</head>
<body>

    <section>
        <h1>FormTracker example</h1>
        <p>This is example page for FormTracker module. In the source code, you can see two separate forms, each with various elements. It is important to notice, that all elements should have proper name attribute and label (or placeholder). It is crucial for further analysis.</p>
        <p>FormTracker will try to guess name of the field in following order:</p>
        <ol>
            <li>Find <code>&lt;label&gt;</code> element that has <code>for</code> attribute or wraps field;</li>
            <li>Check, whether field has <code>aria-labelledby</code> attribute;</li>
            <li>Check, whether field has <code>placeholder</code>;</li>
            <li>Use <code>name</code> attribute;</li>
            <li>If none of above works - skip this field.</li>
        </ol>
        <p>FormTracker catches following events:</p>
        <ul>
            <li>Hover on field</li>
            <li>Focus on field</li>
            <li>Blur on field</li>
            <li>Form submit</li>
        </ul>
        <p>Following data is collected in order to further analysis:</p>
        <ul>
            <li>Number of focus events for each field</li>
            <li>Amount of time between event occurances (hover to focus, focus to blur)</li>
            <li>Total time spent inside field</li>
            <li>Total time needed to fill the form:
                <ul>
                    <li>from the page opening</li>
                    <li>from the first interaction</li>
                </ul>
            </li>
        </ul>

        <form action='/first/fake/action' method='post'>
            <h2>Enter admin panel</h2>
            <p><label for='username'>Username:</label></p>
            <p><input type='text' name='username' id='username' placeholder='Your unique nickname'></p>

            <p><label for='password'>Password:</label></p>
            <p><input type='password' name='password' id='password' placeholder='Your secret password'></p>

            <p><input type='submit' value='Enter'></p>
        </form>

        <form action='/second/fake/action' method='post'>
            <h2>Add an article</h2>
            <p><label for='title'>Title:</label></p>
            <p><input type='text' name='title' id='title' placeholder='Keep it catchy, eg. Is Elvis alive?'></p>

            <p><label id='categoryLabel'>Category:</label></p>
            <p>
                <select name='category' id='category' aria-labelledby='categoryLabel'>
                    <option>Select category</option>
                    <option>Music</option>
                    <option>Movies</option>
                    <option>Books</option>
                </select>
            </p>

            <p><label for='body'>Article body:</label></p>
            <p><textarea name='body' id='body'></textarea></p>

            <p><label for='cover'>Cover image:</label></p>
            <p><input type='file' name='cover' id='cover'></p>

            <p><label>Allow comments:</label></p>
            <p>
                <label><input value='yes' type='radio' name='comments'> Yes</label>
                <label><input value='no' type='radio' name='comments'> No</label>
            </p>

            <p><input type='submit' value='Enter'></p>
        </form>

    </section>

    <script>

        (function(tid, turl, surl) {
            var script = document.createElement('script');
            script.src = surl + 'FormTracker.js';
            document.getElementsByTagName('head')[0].appendChild(script);
            script.onload = function() {
                FormTracker.setup(tid, turl, surl);
            };
        }('1b6453892473a467d07372d45eb05abc', 'localhost:8000/example/second', 'http://localhost:8000/js/'));

    </script>
</body>
</html>
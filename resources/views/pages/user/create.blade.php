@extends('layouts.session')

@section('meta.title') - Create account @stop

@section('content')
    <h2>Create account</h2>

    {!! Form::open(['class' => 'form', 'route' => 'admin.user.store']) !!}

        <div class='field'>
            <label for='email'>E-mail:</label>
            <input type='email' value='' name='email' required>
        </div>

        <div class='field'>
            <label for='password'>Password:</label>
            <input type='password' value='' name='password' required>
        </div>

        <div class='field centered'>
            <input type='submit' value='Create'>
        </div>

        <ul class='help'>
            <li>Already a member? <a href='{{ route('admin.session.create') }}'>Sign in</a></li>
        </ul>

    </form>
@stop


@section('footer.javascripts')
    <script src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
@stop
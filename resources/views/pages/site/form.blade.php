@extends('layouts.admin')

@section('meta.title') - Sites - Details @stop

@section('header.title')
    <a href='{{ route('admin.site.index') }}' class='sub'>Sites</a>
    <span class='sub'>{{ $site->title }}</span>
@stop

@section('content')

    <section id='fields'>
    
        @foreach($statistics as $form) 

            <div class='fields-form'>

                <div class='fields-header'>
                    <div class='fields-header-name'>
                        <h2>Form action:</h2>
                        <p>{{ $form['action']->action }}</p>
                    </div>
                    <div class='fields-header-count'>
                        <h2>Number of fields:</h2>
                        <p>{{ count($form['labels']) }}</p>
                    </div>
                </div>

                <div class='fields-list'>

                    @foreach($form['labels'] as $formLabel)
                        <div class='fields-single'>
                            <div class='fields-single-name'>
                                <h3>Field name:</h3>
                                <p>{{ $formLabel['label']->name }}</p>
                            </div>
                            <div class='fields-single-label'>
                                <h3>Label:</h3>
                                <p>{{ $formLabel['label']->label }}</p>
                            </div>
                            <div class='field-single-data'>
                                <h3>Time:</h3>
                                @if($formLabel['statistics']['count'] > 0)
                                    <ul>
                                        <li>
                                            <span class='label'>Min:</span> 
                                            {{ number_format($formLabel['statistics']['min'], 2) }}<span class='unit'>s</span>
                                        </li>
                                        <li>
                                            <span class='label'>Max:</span> 
                                            {{ number_format($formLabel['statistics']['max'], 2) }}<span class='unit'>s</span>
                                        </li>
                                        <li>
                                            <span class='label'>Average:</span> 
                                            {{ number_format($formLabel['statistics']['avg'], 2) }}<span class='unit'>s</span>
                                        </li>
                                        <li>
                                            <span class='label'>Median:</span>
                                            {{ number_format($formLabel['statistics']['median'], 2) }}<span class='unit'>s</span>
                                        </li>
                                    </ul>
                                @else
                                    No data
                                @endif
                            </div>
                            <div class='fields-single-histogram'>
                                @if($formLabel['statistics']['count'] > 0)
                                    <button 
                                        class='fields-histogram-show' 
                                        data-title='{{ $formLabel['label']->name }}' 
                                        data-bins='{{ json_encode($formLabel['statistics']['histogram']) }}' 
                                        data-min='{{ number_format($formLabel['statistics']['min'], 2) }}' 
                                        data-max='{{ number_format($formLabel['statistics']['max'], 2) }}'
                                    >Histogram</button>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    {{--@foreach($action['fields'] as $field)
                        {{ $field->name }}
                    @endforeach--}}

                </div>

            </div>

        @endforeach

    </section>

    <div id='histogram-wrapper' class='hidden'>
        <div class='histogram-wrapper-inside'>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 800 500" id="histogram">
                
                <g class="grid grid-x">
                    <line x1="100" x2="100" y1="25" y2="450"/>
                </g>
                
                <g class="grid grid-y">
                    <line x1="100" x2="775" y1="450" y2="450"/>
                </g>

                <g class="labels labels-x" id="histogram-labels-x">
                    <text x="430" y="490" class="label label-title">Time (seconds)</text>
                    <text x="100" y="470" class="label" data-var="label.x[0]">0</text>
                    <text x="265" y="470" class="label" data-var="label.x[1]">25</text>
                    <text x="430" y="470" class="label" data-var="label.x[2]">50</text>
                    <text x="605" y="470" class="label" data-var="label.x[3]">75</text>
                    <text x="770" y="470" class="label" data-var="label.x[4]">100</text>
                </g>

                <g class="labels labels-y">
                    <text x="90" y="30" class="label" data-var="label.y.max">100</text>
                    <text x="90" y="225" class="label" data-var="label.y.middle">50</text>
                    <text x="90" y="455" class="label" data-var="label.y.zero">0</text>
                    <text x="50" y="225" class="label label-title">#</text>
                </g>

                <g class="bars"></g>
            </svg>
        </div>
    </div>

@stop


@section('footer.javascripts')

    <script src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
    <script src='{{ asset('js/VisualizationForm.js') }}'></script>
    <script>

        var $histogram = $('#histogram-wrapper');

        $('.fields-histogram-show').on('click', function(e) {
            var $this = $(this),
                array = $this.data('bins');

            FormVisualization.drawHistogram(array, $histogram.find('#histogram'));
            $histogram.removeClass('hidden');
        });

        $histogram.on('click', function(e) {
            var $target = $(e.target);
            if($target.parents('#histogram').length === 0 && $target.attr('id') !== 'histogram') {
                $histogram.addClass('hidden');
            }
        });

    </script>

@stop
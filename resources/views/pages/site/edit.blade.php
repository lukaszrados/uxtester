@extends('layouts.admin')

@section('meta.title') - Sites - Edit site @stop

@section('header.title')
    <a href='{{ route('admin.site.index') }}' class='sub'>Sites</a>
@stop

@section('content')

    <h1>Edit site</h1>

    {!! Form::open(['url' => route('admin.site.update', $site->id), 'method' => 'patch', 'class' => 'form']) !!}
        <div class='field'>
            <label for='url'>Site url (without &quot;http://&quot;):</label>
            <input type='text' id='url' name='url' value='{{ $site->url }}' placeholder='eg. google.com' required>
        </div>

        <div class='field'>
            <label for='title'>Title (displayed only in admin panel):</label>
            <input type='text' id='title' name='title' value='{{ $site->title }}' placeholder='eg. Contact page at my homepage' required>
        </div>

        <div class='field'>
            <label for='module_id'>Module (you can add more modules by creating new sites):</label>
            {!! Form::select('module_id', [null => 'Module'] + $modules, $site->module_id, ['i
            d' => 'module', 'required']) !!}
        </div>

        <div class='field breakpoints {{ $site->module_id === App\Module::MOUSE ? '' : 'hidden' }}'>
            <label>Breakpoints (pixels):</label>
            
            <div class='breakpoints-list'>
                @foreach($site->breakpoints as $breakpoint)
                    <div class='breakpoints-form'>
                        <button class='remove' aria-label='Delete breakpoint'>&times;</button>
                        <input type='hidden' name='breakpoint_id[]' value='{{ $breakpoint->id }}'>
                        <input type='number' name='breakpoint_min[]' placeholder='eg. 720' value='{{ $breakpoint->min_width }}'><span class='unit'>px</span>&nbsp;<span class='dash'>&ndash;</span>&nbsp;<input type='number' name='breakpoint_max[]' placeholder='eg. 1260' value='{{ $breakpoint->max_width >= $max_width ? '' : $breakpoint->max_width }}'><span class='unit'>px</span>
                    </div>
                @endforeach
            </div>

            <div>
                <button class='button breakpoints-add'>Add breakpoint</button>
            </div>
        </div>

        <div class='field separated'>
            <input type='submit' value='Save'>
        </div>
    </form>

    <div class='breakpoints-form' style='display:none;' id='breakpoints-template'>
        <button class='remove' aria-label='Delete breakpoint'>&times;</button>
        <input type='hidden' name='breakpoint_id[]'>
        <input type='number' name='breakpoint_min[]' placeholder='eg. 720'><span class='unit'>px</span>&nbsp;<span class='dash'>&ndash;</span>&nbsp;<input type='number' name='breakpoint_max[]' placeholder='eg. 1260'><span class='unit'>px</span>
    </div>

@stop


@section('footer.javascripts')

    <script src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
    <script>

        var $field      = $('.field.breakpoints'),
            $list       = $('.breakpoints-list'),
            $template   = $('#breakpoints-template');

        $('#module').on('change', function(e) {
            var module = $(this).find('option:selected').text();
            if(module === 'MouseTracker') {
                $field.removeClass('hidden');
            }
            else {
                $field.addClass('hidden');
            }
        });

        $('.breakpoints-add').on('click', function(e) {
            e.preventDefault();

            $template
                .clone()
                .removeAttr('id')
                .css('display', 'block')
                .appendTo($list);
        });

        $list.on('click', '.remove', function(e) {
            e.preventDefault();
            $(this).parents('.breakpoints-form').remove();
        });
    </script>
@stop
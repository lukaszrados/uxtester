@extends('layouts.admin')

@section('meta.title') - Sites @stop
@section('header.title')<span class='sub'>Sites</span>@stop

@section('header.navigation')

    <ul>
        <li><a href='{{ route('admin.site.create') }}'>Add site</a></li>
    </ul>

@stop

@section('content')

    <h1>Sites list</h1>

    <h2>MouseTracker</h2>

    <table class='data'>
        <thead>
            <tr>
                <th class='index'>#</th>
                <th>Site title / url</th>
                <th class='actions'>Actions</th>
            </tr>
        </thead>
        <tbody>
            @if($mouse->count() === 0) 
                <tr>
                    <td colspan='3' class='data-no-rows'>No sites being tracked. <a href='{{ route('admin.site.create') }}'>Add first site</a></td>
                </tr>
            @endif

            <?php $index = 1; ?>

            @foreach($mouse as $site)
                <tr>
                    <td class='index'>{{ $index++ }}</td>
                    <td>
                        {{ $site->title }}
                        <span class='url'>{{ $site->url }}</span>
                    </td>
                    <td class='actions'>
                        <a href='{{ route('admin.site.click', $site->id) }}'>View</a>
                        <a href='{{ route('admin.site.code', $site->id) }}'>Code</a>
                        <a href='{{ route('admin.site.edit', $site->id) }}'>Edit</a>
                        <form action='{{ route('admin.site.destroy', $site->id) }}' method='post'>
                            <input type='hidden' name='_method' value='DELETE'>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <button class='danger'>Remove</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h2>FormTracker</h2>

    <table class='data'>
        <thead>
            <tr>
                <th class='index'>#</th>
                <th>Site title / url</th>
                <th class='actions'>Actions</th>
            </tr>
        </thead>
        <tbody>
            @if($form->count() === 0) 
                <tr>
                    <td colspan='3' class='data-no-rows'>No sites being tracked. <a href='{{ route('admin.site.create') }}'>Add first site</a></td>
                </tr>
            @endif

            <?php $index = 1; ?>

            @foreach($form as $site)
                <tr>
                    <td class='index'>{{ $index++ }}</td>
                    <td>
                        {{ $site->title }}
                        <span class='url'>{{ $site->url }}</span>
                    </td>
                    <td class='actions'>
                        <a href='{{ route('admin.site.form', $site->id) }}'>View</a>
                        <a href='{{ route('admin.site.code', $site->id) }}'>Code</a>
                        <a href='{{ route('admin.site.edit', $site->id) }}'>Edit</a>
                        <a href='{{ route('admin.site.destroy', $site->id) }}' class='danger'>Remove</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@stop
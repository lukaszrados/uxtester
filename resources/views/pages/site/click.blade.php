@extends('layouts.admin')

@section('meta.title') - Sites - Details @stop

@section('header.title')
    <a href='{{ route('admin.site.index') }}' class='sub'>Sites</a>
    <span class='sub'>{{ $site->title }}</span>
@stop

@section('header.settings')
    @parent
@stop

@section('header.navigation')

    <ul id='maps-tabs'>
        <li><a href='#screenshot' data-canvas=''>Screenshot</a></li>
        <li><a href='#clicks-heatmap' data-canvas='clicks-heatmap'>Click Heatmap</a></li>
        <li><a href='#clicks-confetti' data-canvas='clicks-confetti'>Click Confetti</a></li>
        <li><a href='#moves-heatmap' data-canvas='moves-heatmap'>Moves Heatmap</a></li>
        <li><a href='#scroll-heatmap' data-canvas='scroll-heatmap'>Scroll Heatmap</a></li>
    </ul>

@stop

@section('content.before')

    <form method='get' action='{{ route('admin.site.click', $site->id) }}' class='filters'>
        <div class='filter width'>
            <span class='label'>Breakpoint:</span>
            <span class='controls'>
                {!! Form::select('breakpoint', [null => 'All points'] + $breakpoints, $breakpoint ? $breakpoint->id : null, ['class' => 'small']) !!}
            </span>
        </div>
        <div class='filter confirm'>
            <span class='label'>&nbsp;</span>
            <span class='controls'>
                <input type='submit' value='Zatwierdź'>
            </span>
        </div>

        <button id='export'>Export as PNG</button>
    </form>

@stop

@section('content.class')
    full-width
@stop

@section('content')

    <div class='map'>
        @if($breakpoint)
            <img src='{{ asset('uploads/' . $breakpoint->id . '.png') }}' alt='' id='preview'>
        @else
            <h2>Choose breakpoint to see visualizations.</h2>
        @endif
        <canvas id='graph-clicks-heatmap' class='hidden'></canvas>
        <canvas id='graph-moves-heatmap' class='hidden'></canvas>
        <canvas id='graph-clicks-confetti' class='hidden'></canvas>
        <canvas id='graph-scroll-heatmap' class='hidden'></canvas>
        <div class='loading hidden'>
            <p class='loading-text'>
                Trwa generowanie... Proszę czekać...
            </p>
        </div>
    </div>

@stop


@section('footer.javascripts')

    <script src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
    <script src='{{ asset('js/VisualizationClick.js') }}'></script>
    <script>

        var points      = JSON.parse('{!! $points !!}'),
            scrolls     = JSON.parse('{!! $scrolls !!}'),
            moves       = JSON.parse('{!! $moves !!}'),
            preview     = document.getElementById('preview'),
            $map        = $('.map'),
            $loading    = $map.find('.loading');

        $('#maps-tabs a').on('click', function(e) {

            var $this = $(this),
                func = $this.attr('href'),
                isGenerated = !!$this.data('generated'),
                canvasName = $this.data('canvas');

            if(canvasName.length === 0) {
                $map.find('canvas').addClass('hidden');
                return;
            }

            if(isGenerated) {
                $map
                    .find('canvas')
                        .addClass('hidden')
                        .end()
                    .find('#graph-' + canvasName)
                        .removeClass('hidden');

                return;
            }
            
            var canvas = document.getElementById('graph-' + canvasName);
            $loading.removeClass('hidden');

            setTimeout(function() {
                switch(func) {
                    case '#clicks-heatmap':
                        ClickVisualization.init({
                            points:     points,
                            preview:    preview,
                            canvas:     canvas,
                            setType:    'clicks'
                        });
                        ClickVisualization.drawHeatmap();
                    break;

                    case '#moves-heatmap':
                        ClickVisualization.init({
                            points:     moves,
                            preview:    preview,
                            canvas:     canvas,
                            setType:    'moves'
                        });
                        ClickVisualization.drawHeatmap();
                    break;

                    case '#clicks-confetti':

                        ClickVisualization.init({
                            points:     points,
                            preview:    preview,
                            canvas:     canvas,
                            setType:    'clicks'
                        });
                        ClickVisualization.drawConfetti();
                    break;

                    case '#scroll-heatmap':
                        ClickVisualization.init({
                            points:     scrolls,
                            preview:    preview,
                            canvas:     canvas,
                            setType:    'scrolls'
                        });
                        ClickVisualization.drawScrollHeatmap();
                    break;

                    default:
                        
                    break;
                }

                $this.attr('data-generated', true);
                $map
                    .find('canvas')
                        .addClass('hidden')
                        .end()
                    .find('#graph-' + canvasName)
                        .removeClass('hidden');

                $loading.addClass('hidden');

            }, 30);

        });  


        /*
            Exports heatmap and screenshot as PNG file.
            eg.: exportMap(preview, canvas, .3);
            
            param preview   <img> element with screenshot
            param canvas    <canvas> element with heatmap
            param alpha     alpha channel of heatmap 
        */

        $('#export').on('click', function(e) {

            e.preventDefault();
            var canvas = $('.map').find('canvas:not(.hidden)').get(0);
            exportMap(preview, canvas, .3);

        });

        function exportMap(preview, canvas, alpha) {

            var save = document.createElement('canvas'), 
                ctx = save.getContext('2d');

            save.width = preview.width;
            save.height = preview.height;
            ctx.drawImage(preview, 0, 0);

            var imageData = ctx.getImageData(0, 0, save.width, save.height),
                imageDataArray = imageData.data,
                heatmapDataArray = canvas.getContext('2d').getImageData(0, 0, save.width, save.height).data;

            for(var y = 0; y < save.height; ++y) {
                for(var x = 0; x < save.width; ++x) {

                    var index = (y * save.width + x) * 4;
                    for(var k = 0; k < 4; ++k) {
                        imageDataArray[index + k] = parseInt(alpha * imageDataArray[index + k] + (1 - alpha) * heatmapDataArray[index + k]);
                    }

                }
            }

            ctx.putImageData(imageData, 0, 0);

            window.open(save.toDataURL('image/png'));

        }

    </script>

@stop
@extends('layouts.admin')

@section('meta.title') - Sites - Tracking code @stop

@section('header.title')
    <a href='{{ route('admin.site.index') }}' class='sub'>Sites</a>
    <span class='sub'>{{ $site->title }}</span>
@stop

@section('header.settings')
    @parent
@stop

@section('content')
    <h1>Tracking code</h1>

    <p>To install tracking script on your page copy code below and paste it anywhere in your site's code (preferably at the bottom of the page, just before <code>&lt;/body&gt;</code>.</p>

    @if($site->module_id === \App\Module::MOUSE)

    <pre>(function(tid, turl, surl) {
    var script = document.createElement('script');
    script.src = surl + 'js/MouseTracker.js';
    document.getElementsByTagName('head')[0].appendChild(script);
    script.onload = function() {
        MouseTracker.setup(tid, turl, surl);
    };
}('{{ $site->track_id }}', '{{ $site->url }}', '{{ url('/') . '/' }}'));</pre>
    
    @elseif($site->module_id === \App\Module::FORM)

    <pre>(function(tid, turl, surl) {
    var script = document.createElement('script');
    script.src = surl + 'js/FormTracker.js';
    document.getElementsByTagName('head')[0].appendChild(script);
    script.onload = function() {
        FormTracker.setup(tid, turl, surl);
    };
}('{{ $site->track_id }}', '{{ $site->url }}', '{{ url('/') . '/' }}'));</pre>

    @endif

@stop
@extends('layouts.session')

@section('meta.title') - Sign in @stop

@section('content')
    <h2>Sign in</h2>

    @if(session('error'))
        {{ session('error') }}
    @endif

    {!! Form::open(['class' => 'form', 'route' => 'admin.session.store']) !!}

        <div class='field'>
            <label for='email'>E-mail:</label>
            <input type='email' value='' name='email'>
        </div>

        <div class='field'>
            <label for='password'>Password:</label>
            <input type='password' value='' name='password'>
        </div>

        <div class='field centered'>
            <input type='submit' value='Sign in'>
        </div>

        <ul class='help'>
            <li>New to UX Tester? <a href='{{ route('admin.user.create') }}'>Create account</a></li>
            <li><a href='#'>Reset password</a></li>
        </ul>

    </form>
@stop


@section('footer.javascripts')
    <script src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
@stop
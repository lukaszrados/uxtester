<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>

	<title>Admin Panel @yield('meta.title')</title>

    <link rel='stylesheet' href='{{ asset('css/app.css') }}'>

</head>
<body>
	
    <header id='globalheader'>
        <div class='row'>
            <h1><a href='{{ route('home') }}'>Admin Panel</a> @yield('header.title')</h1>
            @section('header.settings')<button class='settings'>Settings</button>@show
        </div>
        <nav class='row navigation'>@yield('header.navigation')</nav>
    </header>

    @yield('content.before')

    <div id='content' class='@yield('content.class')'>
	   @yield('content')
    </div>

    @yield('footer.javascripts')

</body>
</html>

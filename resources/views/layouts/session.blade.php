<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <title>Admin Panel @yield('meta.title')</title>

    <link rel='stylesheet' href='{{ asset('css/app.css') }}'>

</head>
<body id='session'>
    <div class='container'>
        <div class='inside'>
            <div id='content'>
                @yield('content')
            </div>
        </div>
    </div>

    @yield('footer.javascripts')

</body>
</html>

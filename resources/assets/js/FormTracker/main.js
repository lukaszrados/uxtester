var FormTracker = (function() {

    // Config

    var config = {
        savingIntervalTime: 1500,
        trackId:            '',
        trackUrl:           '',
        scriptDirectory:    '',
        token:              '',
        keys:               ['name', 'action', 'event', 'time']
    };

    // Local variables

    var siteId          = null,
        archive         = [],
        startTime       = (new Date()).getTime(),
        elements        = [],
        forms           = [],
        currentFocus    = null;

    // Other modules

    var _ajax           = AJAX,
        _coder          = Coder;


    /*
        Sends 'hello' message to the server allowing to register visit and incoming url.
        Also, sends forms and inputs list along with names and labels. These names will be stored
        in DB and server will return name => id pairs.
        Creates required event listeners and saving intervals.
    */

    var init = function() {

        _ajax.init('/api/');

        findAllElements();

        _ajax.get('token', function(data) {
            config.token = data.token;

            console.log('TODO: Lista elementów wysyłana na serwer');
            _ajax.post('form/init', {
                track_id:       config.trackId,
                url_host:       window.location.host,
                url_pathname:   window.location.pathname,
                elements:       elementsInfo(),
                forms:          formsActions(),  
                _token:         config.token,
            }, function(data) {
                siteId = data.site_id;
                setTimeout(saveToDatabase, config.savingIntervalTime);
            });
        });

        // Attach events 

        attachEventListeners();

        window.onbeforeunload = function() {
            saveToDatabase();
        };

    };


    /*
        Gets time (ms) from window load. 
    */

    var timestamp = function() {
        return (new Date()).getTime() - startTime;
    };


    /*
        Determines whether passed DOM element has <label> parent.
        @return Matched element or null
    */

    var hasParentLabel = function(element) {
        while(element !== null) {
            if(element.nodeName.toLowerCase() === 'label') return element;
            element = element.parentNode;
        }
        return null;
    };


    /*
        Searches element label until first match in following order:
        1. <label for='element_id'>
        2. Wrapping <label>
        3. aria-labelledby attribute
        4. placeholder attribute
        5. name attribute
        6. null if none of following
    */

    var findLabel = function(element) {
        var elementId = element.getAttribute('id'),
            label = null;


        // Maybe <label for='elementId'> ?
        if(elementId) {
            label = document.querySelector('[for="' + elementId + '"]');
            if(label) return {
                text:       label.innerText,
                accuracy:   9
            };
        }

        // Maybe inside <label> ?
        label = hasParentLabel(element);
        if(label) return {
            text:       label.innerText,
            accuracy:   8
        };

        // Maybe aria-labelledby ?
        elementId = element.getAttribute('aria-labelledby');
        if(elementId) {
            label = document.getElementById(elementId);
            if(label) return {
                text:       label.innerText,
                accuracy:   7
            };
        }

        // Maybe - placeholder ?
        var placeholder = element.getAttribute('placeholder');
        if(placeholder) {
            return {
                text:       placeholder,
                accuracy:   6,
            };
        }

        // Maybe - name ?
        var name = element.getAttribute('name');
        if(name) {
            return {
                text:       name,
                accuracy:   5
            };
        }

        // Not found
        return null;
    };


    /*
        Searches all forms and elements and stores them in 'forms' and 'elements' arrays.
    */ 

    var findAllElements = function() {
        forms = document.querySelectorAll('form');

        for(var i in forms) {
            if(!forms.hasOwnProperty(i)) continue;

            var form = forms[i],
                inputs = form.querySelectorAll('input, select, textarea');

            for(var j in inputs) {
                if(!inputs.hasOwnProperty(j)) continue;

                var label = findLabel(inputs[j]);

                if(label) {
                    elements.push({
                        form:       form,
                        element:    inputs[j],
                        label:      label.text,
                        accuracy:   label.accuracy,
                        name:       inputs[j].getAttribute('name')
                    });
                }
            }
        }

    };


    /*
        Gets forms names.
        Sets info as action1_;_action2_;_action3
    */

    var formsActions = function() {

        var result = [],
            action = '';

        for(var i in forms) {
            if(!forms.hasOwnProperty(i)) continue;
            action = forms[i].getAttribute('action');
            if(action.length > 0) {
                result.push(action);
            }
        }

        return result.join('_;_');

    };


    /*
        Gets forms labels.
        Sets info as name_,_label_,_action_,_accuracy_;_name_,_label_,_action_,_accuracy...
    */

    var elementsInfo = function() {

        var result = [];

        for(var i in elements) {
            if(!elements.hasOwnProperty(i)) continue;
            result.push([
                elements[i].name,
                elements[i].label,
                elements[i].form.getAttribute('action'),
                elements[i].accuracy
            ].join('_,_'));
        }

        return result.join('_;_');

    };


    /*
        Adds event to archive. Function adds event to archive with
        following information:
        - name      element 'name' attribute (for input) or 'action' attribute (for form)
        - action    action of parent's form
        - event     name of the event (eg. blur, focus, submit)
        - time      time (ms) since window load

        @param element      DOM element on that event occured
        @param elementType  form/input
        @param eventType    Type of registered event
    */

    var addEventToArchive = function(element, elementType, eventType) {

        var name = '',
            action = '';

        if(elementType === 'form') {
            action = element.getAttribute('action');
            name = action;
        }
        else {
            action = element.form.getAttribute('action');
            name = element.name;
        }

        archive.push({
            name:       name,
            action:     action,
            event:      eventType,
            time:       timestamp()
        });

    };


    /*
        Attaches event listeners to each form and element. 
    */

    var attachEventListeners = function() {

        // Forms

        for(var i in forms) {
            if(!forms.hasOwnProperty(i)) continue;

            (function(f) {
                f.addEventListener('submit', function(e) {
                    addEventToArchive(f, 'form', 'submit');

                    if(currentFocus) {
                        addEventToArchive(currentFocus, 'field', 'blur');
                    }
                });
            })(forms[i]);
        }

        // Elements

        for(var i in elements) {
            if(!elements.hasOwnProperty(i)) continue;

            (function(el) {

                el.element.addEventListener('focus', function(e) {
                    addEventToArchive(el, 'field', 'focus');
                    currentFocus = el;
                });

                el.element.addEventListener('blur', function(e) {
                    addEventToArchive(el, 'field', 'blur');
                    currentFocus = null;
                });

                el.element.addEventListener('change', function(e) {
                    addEventToArchive(el, 'field', 'change');
                });

                // el.element.addEventListener('keyup', function(e) {
                //     addEventToArchive(el, 'field', 'keyup');
                // });

            })(elements[i]); 

        }

    };


    /* 
        Function sends collected data to the server.
        After positive response it calls itself after config.savingIntervalTime ms.

        @todo All danger cases. What if:
        - Server does not respond (once? twice? ...)
        - Server responds very slow and amount of data to be send starts growing?
        - Server responds with error (eg. 500). Try again?

    */

    var saveToDatabase = function() {

        if(archive.length === 0 || !siteId) {
            setTimeout(saveToDatabase, config.savingIntervalTime); 
            return;
        }

        var slice = archive.slice();
        archive = [];

        _ajax.post('form/save', {
            site_id:    siteId,
            events:     _coder.encode(slice, config.keys),
            _token:     config.token
        }, function(data) {
            setTimeout(saveToDatabase, config.savingIntervalTime); 
        });

    };

    return {

        setup: function(trackId, trackUrl, scriptDirectory) {
            config.trackId = trackId;
            config.trackUrl = trackUrl;
            config.scriptDirectory = scriptDirectory;
            init();
        },

    };

}());
var FormVisualization = (function() {

    var MAX_BAR_WIDTH   = 60,
        MAX_BAR_HEIGHT  = 425;

    var drawHistogram = function(data, $svg) {

        var max         = Math.max(...data),
            bins        = data.length,
            width       = 675,
            binWidth    = (width / bins),
            barWidth    = binWidth < MAX_BAR_WIDTH ? binWidth : MAX_BAR_WIDTH;

        // Y Labels

        $svg
        .find('[data-var="label.y.max"]')
            .text(max.toFixed(0))
            .end()
        .find('[data-var="label.y.middle"]')
            .text((max / 2).toFixed(1));

        // X Labels

        for(var i = 1; i < 5; ++i) {
            $svg.find('[data-var="label.x[' + i + ']"]').text((max / 4 * i).toFixed(2));
        }

        // Bars

        var $bars = $svg.find('.bars');
        $bars.find('rect').remove();

        for(var i = 0; i < bins; ++i) {
            var height = (data[i] / max) * MAX_BAR_HEIGHT;
            $('<rect/>', {
                height:     height,
                x:          100 + i * binWidth + binWidth / 2 - barWidth / 2,
                y:          25 + (MAX_BAR_HEIGHT - height),
                width:      barWidth - 1
            }).appendTo($bars);
        }

        $svg.html($svg.html());

    };

    return {
        drawHistogram: function(data, $svg) {
            drawHistogram(data, $svg);
        }
    };

})();

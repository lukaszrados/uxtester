var MouseTracker = (function() {

    // Config

    var config = {
        savingIntervalTime: 1500,
        eventsTypes: {
            click:  1,
            move:   2
        },
        registerClick:      true,
        registerMove:       false,
        registerScroll:     true,
        trackId:            '',
        trackUrl:           '',
        apiUrl:             '',
        scriptDirectory:    '',
        token:              '',
        clickKeys:          ['x','y','type'],
        scrollKeys:         ['y']
    };

    // Local variables

    var siteId      = null,
        positions   = {
            scroll: [],
            click:  [],
        },
        clicksOnly  = 0,    // counts only clicks
        // lastScrollPosition  = -100,
        saveNewMouseMove    = true,
        previousLocation    = window.location.href,
        savingIntervalObject = null;

    // Other modules

    var _ajax       = AJAX,
        _coder      = Coder;

    /*
        Sends 'hello' message to the server allowing to register visit, incoming url and window dimensions.
        Creates required event listeners and saving intervals.
    */

    var init = function() {

        _ajax.init(config.apiUrl);

        _ajax.get('token', function(data) {
            config.token = data.token;
            _ajax.setToken(config.token);

            _ajax.post('click/init', {
                track_id:       config.trackId,
                url_host:       window.location.host,
                url_pathname:   window.location.pathname,
                width:          window.innerWidth,
                _token:         config.token
            }, function(data) {
                if(data.success) {
                    siteId = data.site_id;
                    savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime);

                    if(!data.has_all_images) {
                        screenshot();
                    }
                }
                else {
                    // console.log('Error');
                }
            });
        });

        if(config.registerClick) {
            document.addEventListener('click', clickCallback);
        }
 
        if(config.registerScroll) {
            setInterval(scrollCallback, 400);
        }

        if(config.registerMove) {
            setInterval(function() {
                saveNewMouseMove = true;
            }, 100);

            document.addEventListener('mousemove', moveCallback);
        }

        window.onbeforeunload = function() {
            saveToDatabase();
        };

        // setInterval(function() {
        //     if(window.location.href !== previousLocation) {
        //         // 1. Przerwij dotychczasowe sledzenie
        //         // 2. Rozpocznij nowe (init())
        //     }
        // }, 100);

    };


    var timestamp = function() {
        return parseInt((new Date()).getTime() / 1000);
    };


    var clickCallback = function(event) {

        var position = cursorPosition(event);
        position.type = config.eventsTypes.click;
        positions.click.push(position);

        window.clearInterval(savingIntervalObject);
        savingIntervalObject = null;
        saveToDatabase();

    };


    var moveCallback = function(event) {

        if(saveNewMouseMove) {
            var position = cursorPosition(event);
            position.type = config.eventsTypes.move;

            positions.click.push(position);
            saveNewMouseMove = false;
        }

    };


    var scrollCallback = function(event) {

        var top = document.getElementsByTagName('body')[0].scrollTop;

        // if(top !== lastScrollPosition) {
            // lastScrollPosition = top;

            positions.scroll.push({
                y:      top
            });
        // }

    };


    /* 
        Function sends collected data to the server.
        After positive response it calls itself after config.savingIntervalTime ms.

        @todo All danger cases. What if:
        - Server does not respond (once? twice? ...)
        - Server responds very slow and amount of data to be send starts growing?
        - Server responds with error (eg. 500). Try again?

    */

    var saveToDatabase = function() {

        if((positions.scroll.length === 0 && positions.click.length === 0) || !siteId) {
            savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime); 
            return;
        }

        var click = positions.click.slice(),
            scroll = positions.scroll.slice();

        positions.click = [];
        positions.scroll = [];

        _ajax.post('click/save', {
            site_id:    siteId,
            width:      window.innerWidth,
            height:     window.innerHeight,
            click:      _coder.encode(click, config.clickKeys),
            scroll:     _coder.encode(scroll, config.scrollKeys),
            _token:      config.token
        }, function(data) {
            savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime); 
        });

    };


    /*
        Cross-browser function to fetch user's cursor position.

        @param event    JS event (click / mousemove)
        @return object  Object containing: x, y (where top left is (0, 0)) and timestamp

    */

    var cursorPosition = function(event) {

        var eventDoc, 
            doc, 
            body;

        event = event || window.event;

        if(event.pageX === null && event.clientX !== null) {

          eventDoc = (event.target && event.target.ownerDocument) || document;
          doc = eventDoc.documentElement;
          body = eventDoc.body;

          event.pageX = event.clientX +
            (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
            (doc && doc.clientLeft || body && body.clientLeft || 0);
          event.pageY = event.clientY +
            (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
            (doc && doc.clientTop  || body && body.clientTop  || 0 );

        }

        return { 
            x:      event.pageX, 
            y:      event.pageY
        };

    };


    /*
        BETA. Function takes screenshot of current page 
        and sends it to the server as base64 png image.

        Includes html2canvas library (https://html2canvas.hertzen.com/).
        Library still has problems with capturing images
        (espacially background-images). It will probably never
        be able to take screenshot including images from
        cross-domains calls.

        In working app, this function will probably be called
        only once by the owner of the site (during the first visit).
    */

    var screenshot = function() {

        // load html2canvas js

        var script = document.createElement( 'script' );
        script.src = config.scriptDirectory + 'html2canvas.js';
        script.onload = function() {

            // console.log('screenshot'); 

            html2canvas(document.body, {

                background: '#ffffff',
                onrendered: function(canvas) {    

                    _ajax.post('click/screenshot', {
                        screen:         canvas.toDataURL('image/png'), 
                        track_id:       config.trackId,
                        width:          window.innerWidth,
                        _token:         config.token
                    }, function(data) {
                        // console.log(data);
                    });
                
                }

            });

        };

        document.getElementsByTagName('head')[0].appendChild(script);

    };


    return {

        setup: function(trackId, trackUrl, scriptDirectory, parameters) {
            config.trackId = trackId;
            config.trackUrl = trackUrl;
            config.scriptDirectory = scriptDirectory + 'js/';
            config.apiUrl = scriptDirectory + 'api/';

            if(parameters) {
                config.registerClick = parameters.registerClick === undefined ? config.registerClick : parameters.registerClick;
                config.registerMove = parameters.registerMove === undefined ? config.registerMove : parameters.registerMove;
                config.registerScroll = parameters.registerScroll === undefined ? config.registerScroll : parameters.registerScroll;
            }
            init();
        },

    };

}());
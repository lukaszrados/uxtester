var Coder = (function() {

    /*
        Encodes array of objects to a string, stripping params' names.
        For simplicity, works only for objects containing only simple types.
        Eg. array [{x: 5, y: 7},{x: 4, y: 2}] is encoded into string '5,7;4,2'.

        @param array        Array of objects
        @param keys         Params' names

        @return string      Encoded string
    */

    var encode = function(array, keys) {

        var result = '',
            obj = null,
            key = null;

        for(var i = 0; i < array.length; ++i) {

            obj = array[i];

            for(var j = 0; j < keys.length; ++j) {
                key = keys[j];

                if(!obj.hasOwnProperty(key)) {
                    console.log('Coder.encode warning: incorrect array of keys passed to function.');
                    console.log('Missing key: ' + key);
                    continue;
                }

                result += obj[key] + ',';
            }

            result = result.substring(0, result.length - 1) + ';';

        }

        return result.substring(0, result.length - 1);

    };


    /*
        Parses string to object.
        For simplicity, works only for objects containing only simple types.
        Eg. string '5,7;4,2' is encoded into array [{x: 5, y: 7},{x: 4, y: 2}].

        @param array        Array of objects
        @param keys         Params' names (must be in exact order as in object passed to encode)

        @return array       Decoded array
    */
    
    var decode = function(string, keys) {

        var result = [],
            objects = [];

        objects = string.split(';');

        for(var i = 0; i < objects.length; ++i) {
            var array = objects[i].split(','),
                obj = {};

            for(var j = 0; j < keys.length; ++j) {
                obj[keys[j]] = array[j];
            }

            result.push(obj);
        }

        return result;

    };


    return {
        encode: encode,
        decode: decode
    };

}());
/*
    Template for script generated for user and attached to his website.
*/

(function(trackId, trackUrl, scriptUrl) {

    // 1. Attach API.
    var script = document.createElement('script');
    script.src = scriptUrl;
    document.getElementsByTagName('head')[0].appendChild(script);

    // 2. Setup
    script.onload = function() {
        MouseTracker.setup(trackId, trackUrl);
    };
    
}('xyz123', 'http://localhost:8000', 'http://localhost:8000/js/MouseTracker.js'));

var Coder = (function() {

    /*
        Encodes array of objects to a string, stripping params' names.
        For simplicity, works only for objects containing only simple types.
        Eg. array [{x: 5, y: 7},{x: 4, y: 2}] is encoded into string '5,7;4,2'.

        @param array        Array of objects
        @param keys         Params' names

        @return string      Encoded string
    */

    var encode = function(array, keys) {

        var result = '',
            obj = null,
            key = null;

        for(var i = 0; i < array.length; ++i) {

            obj = array[i];

            for(var j = 0; j < keys.length; ++j) {
                key = keys[j];

                if(!obj.hasOwnProperty(key)) {
                    console.log('Coder.encode warning: incorrect array of keys passed to function.');
                    console.log('Missing key: ' + key);
                    continue;
                }

                result += obj[key] + ',';
            }

            result = result.substring(0, result.length - 1) + ';';

        }

        return result.substring(0, result.length - 1);

    };


    /*
        Parses string to object.
        For simplicity, works only for objects containing only simple types.
        Eg. string '5,7;4,2' is encoded into array [{x: 5, y: 7},{x: 4, y: 2}].

        @param array        Array of objects
        @param keys         Params' names (must be in exact order as in object passed to encode)

        @return array       Decoded array
    */
    
    var decode = function(string, keys) {

        var result = [],
            objects = [];

        objects = string.split(';');

        for(var i = 0; i < objects.length; ++i) {
            var array = objects[i].split(','),
                obj = {};

            for(var j = 0; j < keys.length; ++j) {
                obj[keys[j]] = array[j];
            }

            result.push(obj);
        }

        return result;

    };


    return {
        encode: encode,
        decode: decode
    };

}());
var AJAX = (function() {

    var API_URL = '',
        TOKEN = '';


    /*
        Function performing basic GET request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var get = function(url, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest();

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState == 4) {

                if(httpObject.status == 200) {
                    var data = JSON.parse(httpObject.responseText);
                    successCallback(data);
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('GET', API_URL + url, true);
        httpObject.send(null);

    };


    /*
        Function performing basic POST request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param data             Object consiting data to send
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var post = function(url, data, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest(),
            dataString = '',
            dataParts = [];

        // Converts data array to string (&key1=value1&key2=value2)

        for(var name in data) {
            dataParts.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
        }

        dataString = dataParts.join('&').replace(/%20/g, '+');

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState === 4) {

                if(httpObject.status === 200) {
                    successCallback(JSON.parse(httpObject.responseText));
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('POST', API_URL + url);
        httpObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpObject.setRequestHeader('X-CSRF-Token', TOKEN);
        httpObject.send(dataString);

    };


    return {
        init: function(apiUrl) {
            API_URL = apiUrl;
        },

        setToken: function(t) {
            TOKEN = t;
        },

        get: function(url, successCallback, failureCallback) {
            get(url, successCallback, failureCallback);
        },

        post: function(url, data, successCallback, failureCallback) {
            post(url, data, successCallback, failureCallback);
        },
    };

}());
var MouseTracker = (function() {

    // Config

    var config = {
        savingIntervalTime: 1500,
        eventsTypes: {
            click:  1,
            move:   2
        },
        registerClick:      true,
        registerMove:       false,
        registerScroll:     true,
        trackId:            '',
        trackUrl:           '',
        apiUrl:             '',
        scriptDirectory:    '',
        token:              '',
        clickKeys:          ['x','y','type'],
        scrollKeys:         ['y']
    };

    // Local variables

    var siteId      = null,
        positions   = {
            scroll: [],
            click:  [],
        },
        clicksOnly  = 0,    // counts only clicks
        // lastScrollPosition  = -100,
        saveNewMouseMove    = true,
        previousLocation    = window.location.href,
        savingIntervalObject = null;

    // Other modules

    var _ajax       = AJAX,
        _coder      = Coder;

    /*
        Sends 'hello' message to the server allowing to register visit, incoming url and window dimensions.
        Creates required event listeners and saving intervals.
    */

    var init = function() {

        _ajax.init(config.apiUrl);

        _ajax.get('token', function(data) {
            config.token = data.token;
            _ajax.setToken(config.token);

            _ajax.post('click/init', {
                track_id:       config.trackId,
                url_host:       window.location.host,
                url_pathname:   window.location.pathname,
                width:          window.innerWidth,
                _token:         config.token
            }, function(data) {
                if(data.success) {
                    siteId = data.site_id;
                    savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime);

                    if(!data.has_all_images) {
                        screenshot();
                    }
                }
                else {
                    // console.log('Error');
                }
            });
        });

        if(config.registerClick) {
            document.addEventListener('click', clickCallback);
        }
 
        if(config.registerScroll) {
            setInterval(scrollCallback, 400);
        }

        if(config.registerMove) {
            setInterval(function() {
                saveNewMouseMove = true;
            }, 100);

            document.addEventListener('mousemove', moveCallback);
        }

        window.onbeforeunload = function() {
            saveToDatabase();
        };

        // setInterval(function() {
        //     if(window.location.href !== previousLocation) {
        //         // 1. Przerwij dotychczasowe sledzenie
        //         // 2. Rozpocznij nowe (init())
        //     }
        // }, 100);

    };


    var timestamp = function() {
        return parseInt((new Date()).getTime() / 1000);
    };


    var clickCallback = function(event) {

        var position = cursorPosition(event);
        position.type = config.eventsTypes.click;
        positions.click.push(position);

        window.clearInterval(savingIntervalObject);
        savingIntervalObject = null;
        saveToDatabase();

    };


    var moveCallback = function(event) {

        if(saveNewMouseMove) {
            var position = cursorPosition(event);
            position.type = config.eventsTypes.move;

            positions.click.push(position);
            saveNewMouseMove = false;
        }

    };


    var scrollCallback = function(event) {

        var top = document.getElementsByTagName('body')[0].scrollTop;

        // if(top !== lastScrollPosition) {
            // lastScrollPosition = top;

            positions.scroll.push({
                y:      top
            });
        // }

    };


    /* 
        Function sends collected data to the server.
        After positive response it calls itself after config.savingIntervalTime ms.

        @todo All danger cases. What if:
        - Server does not respond (once? twice? ...)
        - Server responds very slow and amount of data to be send starts growing?
        - Server responds with error (eg. 500). Try again?

    */

    var saveToDatabase = function() {

        if((positions.scroll.length === 0 && positions.click.length === 0) || !siteId) {
            savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime); 
            return;
        }

        var click = positions.click.slice(),
            scroll = positions.scroll.slice();

        positions.click = [];
        positions.scroll = [];

        _ajax.post('click/save', {
            site_id:    siteId,
            width:      window.innerWidth,
            height:     window.innerHeight,
            click:      _coder.encode(click, config.clickKeys),
            scroll:     _coder.encode(scroll, config.scrollKeys),
            _token:      config.token
        }, function(data) {
            savingIntervalObject = setTimeout(saveToDatabase, config.savingIntervalTime); 
        });

    };


    /*
        Cross-browser function to fetch user's cursor position.

        @param event    JS event (click / mousemove)
        @return object  Object containing: x, y (where top left is (0, 0)) and timestamp

    */

    var cursorPosition = function(event) {

        var eventDoc, 
            doc, 
            body;

        event = event || window.event;

        if(event.pageX === null && event.clientX !== null) {

          eventDoc = (event.target && event.target.ownerDocument) || document;
          doc = eventDoc.documentElement;
          body = eventDoc.body;

          event.pageX = event.clientX +
            (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
            (doc && doc.clientLeft || body && body.clientLeft || 0);
          event.pageY = event.clientY +
            (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
            (doc && doc.clientTop  || body && body.clientTop  || 0 );

        }

        return { 
            x:      event.pageX, 
            y:      event.pageY
        };

    };


    /*
        BETA. Function takes screenshot of current page 
        and sends it to the server as base64 png image.

        Includes html2canvas library (https://html2canvas.hertzen.com/).
        Library still has problems with capturing images
        (espacially background-images). It will probably never
        be able to take screenshot including images from
        cross-domains calls.

        In working app, this function will probably be called
        only once by the owner of the site (during the first visit).
    */

    var screenshot = function() {

        // load html2canvas js

        var script = document.createElement( 'script' );
        script.src = config.scriptDirectory + 'html2canvas.js';
        script.onload = function() {

            // console.log('screenshot'); 

            html2canvas(document.body, {

                background: '#ffffff',
                onrendered: function(canvas) {    

                    _ajax.post('click/screenshot', {
                        screen:         canvas.toDataURL('image/png'), 
                        track_id:       config.trackId,
                        width:          window.innerWidth,
                        _token:         config.token
                    }, function(data) {
                        // console.log(data);
                    });
                
                }

            });

        };

        document.getElementsByTagName('head')[0].appendChild(script);

    };


    return {

        setup: function(trackId, trackUrl, scriptDirectory, parameters) {
            config.trackId = trackId;
            config.trackUrl = trackUrl;
            config.scriptDirectory = scriptDirectory + 'js/';
            config.apiUrl = scriptDirectory + 'api/';
            init();
        },

    };

}());
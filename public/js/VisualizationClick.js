var AJAX = (function() {

    var API_URL = '',
        TOKEN = '';


    /*
        Function performing basic GET request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var get = function(url, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest();

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState == 4) {

                if(httpObject.status == 200) {
                    var data = JSON.parse(httpObject.responseText);
                    successCallback(data);
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('GET', API_URL + url, true);
        httpObject.send(null);

    };


    /*
        Function performing basic POST request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param data             Object consiting data to send
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var post = function(url, data, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest(),
            dataString = '',
            dataParts = [];

        // Converts data array to string (&key1=value1&key2=value2)

        for(var name in data) {
            dataParts.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
        }

        dataString = dataParts.join('&').replace(/%20/g, '+');

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState === 4) {

                if(httpObject.status === 200) {
                    successCallback(JSON.parse(httpObject.responseText));
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('POST', API_URL + url);
        httpObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpObject.setRequestHeader('X-CSRF-Token', TOKEN);
        httpObject.send(dataString);

    };


    return {
        init: function(apiUrl) {
            API_URL = apiUrl;
        },

        setToken: function(t) {
            TOKEN = t;
        },

        get: function(url, successCallback, failureCallback) {
            get(url, successCallback, failureCallback);
        },

        post: function(url, data, successCallback, failureCallback) {
            post(url, data, successCallback, failureCallback);
        },
    };

}());
var ClickVisualization = (function() {

    var points              = [],
        range               = {},
        canvas              = null, 
        preview             = null,
        offScreenContext    = null,
        lookupTable         = [],
        lookupTableSize     = 1000,
        changedCoords       = [];


    /*
        Counts min and max values of 'key' property in array of objects.

        @param array    Array of objects
        @param key      Name of attribute

        @return object  {min,max}
    */

    var countMinMax = function(array, key) {
        var min = Number.POSITIVE_INFINITY,
            max = Number.NEGATIVE_INFINITY;

        for(var i = 0; i < array.length; ++i) {
            if(array[i][key] < min) min = array[i][key];
            if(array[i][key] > max) max = array[i][key];
        }

        return { min: min, max: max };
    };


    /*
        Changes points' x coordinates. It is required for better work with mobile.
        Server sends points with their x set relatively to viewport width.
        Eg. if screen width was 1600px and point was registered at coords (300, 0)
        then it's x position returned by server is: 300 - 1600 / 2 = -500
        (meaning that event took place 500px on the left from image center).

        This function adds (preview.width / 2) to each point
    */

    var changePointsCoordinates = function() {

        for(var i = 0; i < points.length; ++i) {
            points[i].x += preview.width / 2;
        }

    };


    /*
        Counts hue value for hsl/hsv colors.
        Hue range is kept from 0 (pure red) to max (default - 180 - blue).
        The higher value is, the closer to red color is returned.

        @param value    Value (range: <0;1>)
        @param max      Max hue (default: 180 - blue)

        @return         Value of hue
    */

    var hue = function(value, max) {
        var max = max || 180,
            result = parseInt(max - value * max);

        if(result < 0) result = 0;

        return result;
    };


    /*
        Converts color from HSL(A) to RGBA.
        Based on: https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSL

        @param h        Hue <0:360)
        @param s        Saturation <0:1>
        @param l        Lightness <0:1>
        @param alpha    Alpha (optional) <0:1>

        @return array [red, green, blue, alpha] <0:255> 
    */

    var hslToRGB = function(h, s, l, a) {
        var alpha = parseInt(a * 255) || 255,
            r, g, b;

        if(s === 0) {
            r = g = b = l;
        }
        else {
            var c = (1 - Math.abs(2 * l - 1)) * s,
                hp = h / 60.0,
                x = c * (1 - Math.abs(hp % 2 - 1)),
                m = l - 0.5 * c;

            if(hp >= 5) [r, g, b] = [c, 0, x];
            else if(hp >= 4) [r, g, b] = [x, 0, c];
            else if(hp >= 3) [r, g, b] = [0, x, c];
            else if(hp >= 2) [r, g, b] = [0, c, x];
            else if(hp >= 1) [r, g, b] = [x, c, 0];
            else [r, g, b] = [c, x, 0];

            [r, g, b] = [r + m, g + m, b + m];
        }

        return [r * 255, g * 255, b * 255, alpha];
    }

    /*
        Perform linear interpolation and draws clicks heatmap.
    */

    var bilinearInterpolation = function() {

        var range = {
            x: countMinMax(points, 'x'),
            y: countMinMax(points, 'y')
        };

        range.x.max = preview.width;
        range.y.max = preview.height;

        var grid = {
            x:          50,     // Podziel przesterzeń na dyskretną siatkę (x elementów w poziomie)
            y:          0,      // Zostanie obliczone
            px:         0,      // Rozmiar pojedynczego elementu gridu (zostanie obliczony)
            values:     [],
            maxValue:   0
        };

        grid.px = range.x.max / grid.x;                     // Oblicz rozmiar pojedynczego elementu (szerokość / liczba el.)
        grid.y = parseInt(range.y.max / grid.px) + 1;       // Oblicz liczbę elementów w pionie

        // Przygotuj tablicę 2d o rozmiarach (grid.x, grid.y)

        for(var i = 0; i < grid.y + 2; ++i) {
            grid.values.push([]);
            for(var j = 0; j < grid.x + 2; ++j) {
                grid.values[i].push(0);
            }            
        }

        // Przypisz punkty do odpowiadających im miejsc na siatce
        // Każdy punkt przesuwamy o px/2 w każdą stronę
        // Dzięki temu punkty znajdą się w elementach siatki, których środki są im najbliższe

        for(var i = 0; i < points.length; ++i) {
            var point = points[i],
                x = parseInt((point.x + grid.px) / grid.px),
                y = parseInt((point.y + grid.px) / grid.px);

            grid.values[y][x] += 1;
        }

        // Znajdź maksymalną wartość

        for(var x = 0; x < grid.x + 2; ++x) {
            for(var y = 0; y < grid.y + 2; ++y) {
                if(grid.values[y][x] > grid.maxValue) {
                    grid.maxValue = grid.values[y][x];
                }
            }
        }

        // bilinear interpolation

        for(var x = 0; x < grid.x + 1; ++x) {
            for(var y = 0; y < grid.y + 1; ++y) {
                var f11 = grid.values[y][x] / grid.maxValue,
                    f12 = grid.values[y + 1][x] / grid.maxValue,
                    f21 = grid.values[y][x + 1] / grid.maxValue,
                    f22 = grid.values[y + 1][x + 1] / grid.maxValue,

                    x1 = x * grid.px,
                    x2 = (x + 1) * grid.px,
                    y1 = y * grid.px,
                    y2 = (y + 1) * grid.px,

                    factor = 1.0 / (grid.px * grid.px);

                for(var _x = x * grid.px; _x < (x + 1) * grid.px; ++_x) {
                    for(var _y = y * grid.px; _y < (y + 1) * grid.px; ++_y) {
                        var value = f11 * (x2 - _x) * (y2 - _y);
                        value += f21 * (_x - x1) * (y2 - _y);
                        value += f12 * (x2 - _x) * (_y - y1);
                        value += f22 * (_x - x1) * (_y - y1);
                        value *= factor;

                        offScreenContext.fillStyle = 'hsl(' + hue(value, 220) + ', 100%, 50%)';
                        offScreenContext.fillRect(_x - grid.px / 2, _y - grid.px / 2, 1, 1);
                    }
                }


            }
        }

    }


    /*
        Performs gaussian interpolation and draws clicks heatmap.
    */

    var gaussianInterpolation = function() {

        function gs(x, y, x0, y0) {
            var sx = 4.0,
                sy = 4.0;

            return Math.exp(-(Math.pow(x - x0, 2) / (2 * sx * sx) + Math.pow(y - y0, 2) / (2 * sy * sy)));
        }

        var gaussianMask = [],
            gaussianMaskSize = 25;

        // Fill gaussian mask 

        for(var i = 0; i < gaussianMaskSize; ++i) {
            gaussianMask.push([]);
            for(var j = 0; j < gaussianMaskSize; ++j) {
                gaussianMask[i].push(gs(i, j, gaussianMaskSize / 2, gaussianMaskSize / 2));
            }
        }

        // Przygotuj w pamieci obszar do wyrysowania

        var image = {
            width:  preview.width,
            height: preview.height,
            map:    [],
            max:    0
        };

        for(var i = 0; i < image.height; ++i) {
            image.map.push([]);
            for(var j = 0; j < image.width; ++j) {
                image.map[i].push(0);
            }
        }

        // Dodaj do pamięci piksele

        for(var p = 0; p < points.length; ++p) {

            var x = points[p].x,
                y = points[p].y,
                halfMaskSize = parseInt(gaussianMaskSize / 2);

            for(var i = -halfMaskSize; i <= halfMaskSize; ++i) {

                if(y + i < 0 || y + i >= image.height) continue;

                for(var j = -halfMaskSize; j <= halfMaskSize; ++j) {

                    if(x + j < 0 || x + j >= image.width) continue;
                    image.map[y + i][x + j] += gaussianMask[halfMaskSize + i][halfMaskSize + j];

                    if(image.map[y + i][x + j] > image.max) {
                        image.max = image.map[y + i][x + j];
                    }

                }

            }

        }

        // Znormalizuj do zakresu 0..lookupTableSize

        for(var i = 0; i < image.height; ++i) {
            for(var j = 0; j < image.width; ++j) {
                image.map[i][j] = Math.round((image.map[i][j] / image.max) * (lookupTableSize - 1));
            }
        }

        var imageData = offScreenContext.getImageData(0, 0, image.width, image.height),
            imageDataArray = imageData.data;

        // Narysuj heatmapę

        var index, color;

        for(var i = 0; i < image.height; ++i) {
            for(var j = 0; j < image.width; ++j) {

                index = (i * image.width + j) * 4;

                for(var k = 0; k < 4; ++k) {
                    imageDataArray[index + k] = lookupTable[image.map[i][j]][k];
                }

            }
        }

        offScreenContext.putImageData(imageData, 0, 0);

    };


    /*
        Draws 'confetti' map - each has its own point drawn.
        If no color specified - colors are random at each point (recommended).

        @param color    Color of points. If not specified, random colors will be used
    */

    var confetti = function(color) {

        offScreenContext.fillStyle = '#000000';
        offScreenContext.fillRect(0, 0, preview.width, preview.height);

        for(var i = 0; i < points.length; ++i) {
            offScreenContext.fillStyle = color ? color : 'hsl(' + (Math.random() * 360) + ', 100%, 50%)';
            offScreenContext.beginPath();
            offScreenContext.arc(points[i].x, points[i].y, 3, 0, 2 * Math.PI, false);
            offScreenContext.fill();
            offScreenContext.closePath();
        }

    };


    /*
        Draws scrolls heatmap displaying, which parts of website are most - and least - visited.
    */

    var scrollsMap = function() {

        // Przygotuj w pamieci obszar do wyrysowania

        var image = {
            width:  preview.width,
            height: preview.height,
            map:    [],
            max:    0
        };

        for(var i = 0; i < image.height; ++i) {
            image.map.push(0);
        }

        // Dodaj obszary do pamięci

        for(var i = 0; i < points.length; ++i) {
            for(var y = points[i].y; y < points[i].y + points[i].height && y < image.height; ++y) {
                image.map[y] += 1;
            }
        }

        // Znajdź max i znormalizuj

        image.max = Math.max(...image.map);

        for(var i = 0; i < image.height; ++i) {
            image.map[i] /= image.max;
        }

        // Narysuj

        for(var y = 0; y < image.height; ++y) {
            offScreenContext.fillStyle = 'hsl(' + hue(image.map[y], 220) + ', 100%, 50%)';
            offScreenContext.fillRect(0, y, image.width, 1);
        }

    };


    var copyOnScreen = function() {

        var ctx = canvas.getContext('2d'),
            image = offScreenContext.getImageData(0, 0, preview.width, preview.height);

        canvas.width = preview.width;
        canvas.height = preview.height;
        canvas.style.marginLeft = (-preview.width / 2) + 'px';

        ctx.putImageData(image, 0, 0); 

    };


    return {

        /*
            @param params Object {
                points:     [],
                preview:    node [img],
                canvas:     node [canvas],
                setType:    clicks|moves|scrolls
            }
        */
        init: function(params) {

            // Get canvas and preview
            preview = params.preview;
            canvas = params.canvas;

            // Get all points
            points = params.points;

            // If this set (params.setType) not in changedCoords
            if(changedCoords.indexOf(params.setType) === -1) {
                changePointsCoordinates();
                changedCoords.push(params.setType);
            }

            range.x = countMinMax(points, 'x');
            range.y = countMinMax(points, 'y');

            // Prepare off screen canvas (for faster drawing)

            var offScreenCanvas = document.createElement('canvas');
            offScreenCanvas.width = preview.width;
            offScreenCanvas.height = preview.height;
            offScreenContext = offScreenCanvas.getContext('2d');

            // Create global lookup table

            for(var i = 0; i < lookupTableSize; ++i) {
                lookupTable.push(hslToRGB(hue(i / (lookupTableSize - 1), 230), 1, 0.5));
            }

        },


        drawHeatmap: function() {
            // FYI: You can use bilinearInterpolation(), but it gives poor results with the same execution time
            gaussianInterpolation();
            // bilinearInterpolation();
            copyOnScreen();
        },


        drawScrollHeatmap: function() {
            scrollsMap();
            copyOnScreen();
        },


        drawConfetti: function(color) {
            confetti(color);
            copyOnScreen();
        },


        clear: function() {
            points = [];
            range = {};

            if(canvas) {
                canvas.width = 0;
                canvas.height = 0;
                canvas = null; 
            }
            preview = null;
            offScreenContext = null;
        },
    };

})();
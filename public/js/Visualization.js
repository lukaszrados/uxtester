var AJAX = (function() {

    var API_URL = '';


    /*
        Function performing basic GET request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var get = function(url, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest();

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState == 4) {

                if(httpObject.status == 200) {
                    var data = JSON.parse(httpObject.responseText);
                    successCallback(data);
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('GET', API_URL + url, true);
        httpObject.send(null);

    };


    /*
        Function performing basic POST request.
        This function assumes that there are only two possible types of http response:
        - 200 - success
        - any other - failure

        @param url              POST url where data is sent
        @param data             Object consiting data to send
        @param successCallback  Function that is being fired when status = 200
        @param failureCallback  Function that is being fired when status is not 200

        @return void
    */

    var post = function(url, data, successCallback, failureCallback) {

        var httpObject = new XMLHttpRequest(),
            dataString = '',
            dataParts = [];

        // Converts data array to string (&key1=value1&key2=value2)

        for(var name in data) {
            dataParts.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
        }

        dataString = dataParts.join('&').replace(/%20/g, '+');

        httpObject.onreadystatechange = function() {

            if(httpObject.readyState === 4) {

                if(httpObject.status === 200) {
                    successCallback(JSON.parse(httpObject.responseText));
                }
                else {
                    if(failureCallback) {
                        failureCallback(httpObject.status);
                    }
                }

            }

        };

        httpObject.open('POST', API_URL + url);
        httpObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpObject.send(dataString);

    };


    return {
        init: function(apiUrl) {
            API_URL = apiUrl;
        },

        get: function(url, successCallback, failureCallback) {
            get(url, successCallback, failureCallback);
        },

        post: function(url, data, successCallback, failureCallback) {
            post(url, data, successCallback, failureCallback);
        },
    };

}());
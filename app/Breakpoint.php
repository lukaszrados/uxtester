<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Site;

class Breakpoint extends Model {

    const MAX_WIDTH = 5000;
    public $timestamps = false;

    public function site() {
        return $this->belongsTo(Site::class);
    }

    public function getNameAttribute() {
        return $this->min_width . 'px - ' . $this->max_width . 'px';
    }


}
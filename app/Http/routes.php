<?php

/*
    Admin panel
*/

Route::get('hosts/{hostname}', function(Request $request, $hostname) {
    $file = File::get(storage_path() . '/Metadata.js');
    return response()->json(json_decode($file));
});

Route::get('hosts/{hostname}/probes', function(Request $request, $hostname) {
    $file = File::get(storage_path() . '/Probes.js');
    return response()->json(json_decode($file));
});

Route::group(['prefix' => 'admin'], function() {
    
    Route::get('/', ['as' => 'home', 'uses' => 'PageController@index']);

    // Route::get('/', function() {
    //     return redirect()->route('admin.session.create');
    // });

    // Route::resource('session', 'SessionController', ['only' => ['create', 'store', 'destroy']]);
    // Route::resource('user', 'UserController', ['only' => ['create', 'store']]);

    // Route::group(['middleware' => 'auth'], function() {
        // Route::resource('user', 'UserController');
        Route::resource('site', 'SiteController');
        Route::get('site/{id}/click', ['as' => 'admin.site.click', 'uses' => 'SiteController@click']);
        Route::get('site/{id}/form', ['as' => 'admin.site.form', 'uses' => 'SiteController@form']);
        Route::get('site/{id}/code', ['as' => 'admin.site.code', 'uses' => 'SiteController@code']);
    // });

});


/*
    API.
*/

Route::group(['prefix' => 'api'], function() {

    Route::group(['prefix' => 'click'], function() {
        Route::post('init', 'Api\ClickController@init');
        Route::post('save', 'Api\ClickController@save');
        Route::post('screenshot', 'Api\ClickController@screenshot');
    });

    Route::group(['prefix' => 'form'], function() {
        Route::post('init', 'Api\FormController@init');
        Route::post('save', 'Api\FormController@save');
    });
    
    Route::get('token', 'Api\ApiController@token');

});


/*
    Examples.
*/

Route::group(['prefix' => 'example'], function() {

    Route::get('first', function() {
        return response()->view('examples.first');
    });
    Route::get('second', function() {
        return response()->view('examples.second');
    });

});

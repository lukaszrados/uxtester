<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Breakpoint;
use App\Point;
use App\Scroll;
use App\Site;
use App\User;
use App\Visit;
use App\Module;
use App\FormLabel;
use App\FormEvent;
use App\FormAction;

class SiteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$mouse = Site::where('module_id', Module::MOUSE)->get();
		$form = Site::where('module_id', Module::FORM)->get();
        return response()->view('pages.site.index', compact('mouse', 'form'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$modules = Module::lists('name', 'id');
		return response()->view('pages.site.create', compact('modules'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		\DB::beginTransaction();
		try {
			$site = new Site();
			$site->url = $request->input('url');
			$site->module_id = (int)$request->input('module_id');
			$site->user_id = 1;
			$site->title = \Input::get('title');
			$site->save();

			if(!$site) {
				throw new \Exception('Site was not created');
			}

			if($site->module_id === Module::MOUSE) {

				$breakpoints_min = array_map(function($el) { return (int)$el; }, $request->input('breakpoint_min'));
				$breakpoints_max = array_map(function($el) { return (int)$el > 0 ? (int)$el : Breakpoint::MAX_WIDTH; }, $request->input('breakpoint_max'));
				$breakpoints = count($breakpoints_min);

				for($i = 0; $i < $breakpoints; ++$i) {
					$breakpoint = new Breakpoint();
					$breakpoint->site_id = $site->id;
					$breakpoint->min_width = $breakpoints_min[$i];
					$breakpoint->max_width = $breakpoints_max[$i];
					$breakpoint->has_image = false;
					$breakpoint->save();

					if(!$breakpoint) {
						throw new \Exception('Breakpoint ' . ($i + 1) . ' was not created');
					}
				}
			}

			$site->track_id = sha1($site->id);
			$site->save();

			\DB::commit();
			return redirect()->route('admin.site.code', [$site->id]);
		}
		catch(\Exception $e) {
			\DB::rollback();
			dd($e);
		}
	}

	/**
	 * Display site tracked by 'MouseTracker' module.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function click(Request $request, $id)
	{
		$site = Site::where('id', $id)->with('breakpoints')->first();
		$breakpoint_id = (int)$request->input('breakpoint', null);
		$breakpoint = $breakpoint_id ? Breakpoint::where('id', $breakpoint_id)->where('site_id', $site->id)->first() : null;

		if($breakpoint) {
			$resolution = [
				'min' => $breakpoint->min_width,
				'max' => $breakpoint->max_width
			];
			$points = json_encode($site->pointsByResolution($resolution, Point::Click));
			$moves = json_encode($site->pointsByResolution($resolution, Point::Move));
			$scrolls = json_encode($site->scrollsByResolution($resolution));
		}
		else {
			$resolution = ['min' => 0, 'max' => Breakpoint::MAX_WIDTH];
			$points = '';
			$moves = '';
			$scrolls = '';
		}

		$breakpoints = $site->breakpoints->lists('name', 'id');

        return response()->view('pages.site.click', compact('site', 'points', 'scrolls', 'moves', 'breakpoint', 'breakpoints'));
	}

	/**
	 * Display site tracked by 'FormTracker' module.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function form(Request $request, $id)
	{
		$site = Site::where('id', $id)->first();
		$statistics = $site->formStatistics();

        return response()->view('pages.site.form', compact('site', 'statistics'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request, $id)
	{
		$site = Site::where('id', $id)->with('breakpoints')->first();
		$modules = Module::lists('name', 'id');
		$max_width = Breakpoint::MAX_WIDTH;

		return response()->view('pages.site.edit', compact('site', 'modules', 'max_width'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$site = Site::where('id', $id)->first();
		
		if(!$site) {
			throw new \Exception('Site not found');
		}

		$site->url = $request->input('url');
		$site->module_id = (int)$request->input('module_id');
		$site->title = \Input::get('title');
		$site->save();

		if($site->module_id === Module::MOUSE) {
			$breakpoints_id = array_map(function($el) { return (int)$el; }, $request->input('breakpoint_id'));
			$breakpoints_min = array_map(function($el) { return (int)$el; }, $request->input('breakpoint_min'));
			$breakpoints_max = array_map(function($el) { return (int)$el > 0 ? (int)$el : Breakpoint::MAX_WIDTH; }, $request->input('breakpoint_max'));
			$breakpoints = count($breakpoints_min);

			for($i = 0; $i < $breakpoints; ++$i) {
				if($breakpoints_id[$i] > 0) {
					$breakpoint = Breakpoint::where('id', $breakpoints_id[$i])->first();
				}
				else {
					$breakpoint = new Breakpoint();
					$breakpoint->has_image = false;
					$breakpoint->site_id = $site->id;
				}
				$breakpoint->min_width = $breakpoints_min[$i];
				$breakpoint->max_width = $breakpoints_max[$i];
				$breakpoint->save();

				if(!$breakpoint) {
					throw new \Exception('Breakpoint ' . ($i + 1) . ' was not created');
				}
			}
		}
		else {
			Breakpoint::where('site_id', $site->id)->delete();
		}

		\DB::commit();
		return redirect()->route('admin.site.index')->with('information', 'Site ' . $site->title . ' modified');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		$site = Site::where('id', $id)->first();

		$breakpoints = Breakpoint::where('site_id', $id);
		$breakpointsCollection = $breakpoints->get();
		$breakpoints->delete();

		foreach($breakpointsCollection as $breakpoint) {
			$path = './uploads/' . $breakpoint->id . '.png';
			if($breakpoint->has_image && \File::exists($path)) {
				\File::delete($path);
			}
		}

		FormAction::where('site_id', $id)->delete();
		FormEvent::where('site_id', $id)->delete();
		FormLabel::where('site_id', $id)->delete();
		Point::where('site_id', $id)->delete();
		Scroll::where('site_id', $id)->delete();
		Site::destroy($id);

		return redirect()->route('admin.site.index')->with('information', 'Site ' . $site->title . ' deleted');
	}


	public function code(Request $request, $id)
	{
		$site = Site::where('id', $id)->first();
        return response()->view('pages.site.code', compact('site'));
	}

}

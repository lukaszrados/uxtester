<?php namespace App\Http\Controllers\Api;

use App\Breakpoint;
use App\Point;
use App\Scroll;
use App\Site;
use App\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ClickController extends ApiController {

    public function __construct() {
        parent::__construct();
    }

    public function init() 
    {
        $trackId = \Input::get('track_id');
        $urlHost = \Input::get('url_host');
        $urlPathname = \Input::get('url_pathname');
        $width = (int)(\Input::get('width'));

        $site = Site::where('track_id', $trackId)->first();
        if(!$site) {
            return response()->json([
                'success'   => false, 
                'error'     => 'Given track_id = ' . $trackId . ' not found'
            ]);
        }

        $site->visits = $site->visits + 1;
        $site->save();

        return response()->json(['site_id' => $site->id, 'success' => true, 'has_all_images' => $site->hasAllScreenshots($width)]);
    }


    public function save() 
    {
        $siteId = \Input::get('site_id');

        $width = \Input::get('width');
        $height = \Input::get('height');

        $clicks = \Input::get('click');
        $scrolls = \Input::get('scroll');

        $site = Site::where('id', $siteId)->first();

        if(!$site) {
            return response()->json([
                'success'   => false, 
                'error'     => 'Site not found'
            ]);
        }

        if(Str::length($clicks) > 0) {
            $clicks = explode(';', $clicks);
            $clicksToModel = [];

            foreach($clicks as $click) {
                $single = explode(',', $click);

                $clicksToModel[] = [
                    'x'             => $single[0],
                    'y'             => $single[1],
                    'type'          => $single[2],
                    'site_id'       => $siteId,
                    'width'         => $width,
                    'height'        => $height,
                    'created_at'    => date('Y-m-d H:i:s')
                ];
            }

            Point::insert($clicksToModel);
        }

        if(Str::length($scrolls) > 0) {
            $scrolls = explode(';', $scrolls);
            $scrollsToModel = [];

            foreach($scrolls as $scroll) {
                $single = explode(',', $scroll);

                $scrollsToModel[] = [
                    'y'             => $single[0],
                    'width'         => $width,
                    'height'        => $height,
                    'site_id'       => $siteId,
                    'created_at'    => date('Y-m-d H:i:s')
                ];
            }

            Scroll::insert($scrollsToModel);
        }

        return response()->json(['success' => true]);

    }


    public function screenshot()
    {
        $trackId = \Input::get('track_id');
        $width = (int)(\Input::get('width'));
        $screen = \Input::get('screen');
        $site = Site::where('track_id', $trackId)->first();

        if(!$screen) return response()->json(['success' => false, 'error' => 'Screenshot not found']);
        if(!$site) return response()->json(['success' => false, 'error' => 'No matching site found']);
        
        $breakpoints = $site->breakpointsByWidth($width);

        foreach($breakpoints as $breakpoint) {
            $image = fopen(public_path() . '/uploads/' . $breakpoint->id . '.png', 'wb');
            $data = explode(',', $screen);
            fwrite($image, base64_decode($data[1]));
            fclose($image);

            $breakpoint->has_image = true;
            $breakpoint->save();
        }

        return response()->json(['success' => true]);
    }

}
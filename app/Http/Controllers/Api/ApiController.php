<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiController extends Controller {

    public function __construct() 
    {
        // header('Access-Control-Allow-Origin: *');
        // header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
        // header('Access-Control-Allow-Methods: POST, GET');
    }

    public function token()
    {
        return response()->json(['token' => csrf_token()]);
    }

}
<?php namespace App\Http\Controllers\Api;

use App\Site;
use App\User;
use App\FormAction;
use App\FormEvent;
use App\FormLabel;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FormController extends ApiController {

    public function init() 
    {
        $trackId = \Input::get('track_id');
        $urlHost = \Input::get('url_host');
        $urlPathname = \Input::get('url_pathname');

        $site = Site::where('track_id', $trackId)->where('url', $urlHost . $urlPathname)->first();
        if(!$site) {
            return response()->json([
                'success'   => false, 
                'error'     => 'Given track_id = ' . $trackId . ' not found'
            ]);
        }

        // Get forms' actions, check in database if all of them exists (if not => add)
        $forms = explode('_;_', \Input::get('forms'));

        foreach($forms as $form) {
            $action = FormAction::where('site_id', $site->id)->where('action', $form)->first();
            if(!$action && Str::length($form) > 0) {
                $action = new FormAction();
                $action->site_id = $site->id;
                $action->action = $form;
                $action->save();
            }
        }

        // Get elements' labels, check if better are sent (or missing)
        // Gets data as: name_,_label_,_action_,_accuracy_;_name_,_label_,_action_,_accuracy
        $elements = explode('_;_', \Input::get('elements'));

        foreach($elements as $element) {
            $element = explode('_,_', $element);
            $label = FormLabel::where('site_id', $site->id)->where('name', $element[0])->where('action', $element[2])->first();

            if(!$label) {
                $label = new FormLabel();
                $label->site_id     = $site->id;
                $label->name        = $element[0];
                $label->action      = $element[2];
                $label->label       = $element[1];
                $label->accuracy    = $element[3];
                $label->save();
            }
            else if($label->accuracy < (int)($element[3]) && $label->label !== $element[1]) {
                $label->accuracy    = (int)($element[3]);
                $label->label       = $element[1];
                $label->save();
            }
        }

        return response()->json(['site_id' => $site->id, 'success' => true]);
    }


    public function save() 
    {
        $siteId = \Input::get('site_id');
        $events = \Input::get('events');
        $site = Site::where('id', $siteId)->first();

        if(!$site) {
            return response()->json([
                'success'   => false, 
                'error'     => 'Site not found'
            ]);
        }

        if(Str::length($events) > 0) {
            $events = explode(';', $events);

            $focus = 0;
            $blur = 0;

            $toModel = [];

            foreach($events as $event) {
                $single = explode(',', $event);

                $toModel[] = [
                    'name'          => $single[0],
                    'action'        => $single[1],
                    'type'          => FormEvent::typeByName($single[2]),
                    'time'          => $single[3],
                    'site_id'       => $siteId
                ];
            }

            FormEvent::insert($toModel);
        }

        return response()->json(['success' => true]);

    }


}
<?php 

namespace App\Http\Controllers;

use App\Breakpoint;
use App\Point;
use App\Scroll;
use App\Site;
use App\User;
use App\Visit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller {

    public function index() 
    {
        return redirect()->route('admin.site.index');
    }

}
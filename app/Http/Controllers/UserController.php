<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;

class UserController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		return response()->view('pages.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');

		if(!$email || !$password) {
			return redirect()->route('admin.user.create')->with('error', 'E-mail and password are required.');
		}

		if(User::where('email', $email)->first()) {
			return redirect()->route('admin.user.create')->with('error', 'E-mail address already in use.');
		}

		$password = bcrypt($password);
		print_r('before user');

		$user = new User();
		$user->email = $email;
		$user->password = $password;
		$user->save();

		print_r('after user');

		if(Auth::attempt(['email' => $email, 'password' => $password])) {
			dd('zalogowany');
			return redirect()->route('admin.site.index');
		}
		else {
			dd('nie');
			return redirect()->route('admin.session.create');
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request, $id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

}

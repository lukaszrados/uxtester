<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FormEvent extends Model {

    public static $types = [
        'focus'     => 1,
        'blur'      => 2,
        'change'    => 3,
        'submit'    => 4
    ];
    
    public $timestamps  = false;
    protected $table    = 'form_events';
    protected $fillable = ['visit_id', 'type', 'time'];

    public static function typeByName($name) {
        if(array_key_exists($name, self::$types)) return self::$types[$name];
        return null;
    }

    public function site() {
        return $this->belongsTo(Site::class);
    }
}
<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Point extends Model {
	
	const Click = 1;
	const Move = 2;

	public function site() {
		return $this->belongsTo(Site::class);
	}

    public static function countPositionByCenter($x, $width) {
        return (int)($x - $width / 2);
    }

}
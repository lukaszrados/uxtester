<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use App\FormEvent;

class FormLabel extends Model {
    
    public $timestamps  = false;
    protected $table    = 'form_labels';
    protected $fillable = ['site_id', 'name', 'label', 'accuracy'];

    public function site() {
        return $this->belongsTo(Site::class);
    }

    public function statistics($seconds = true) {
        $events = FormEvent::where('site_id', $this->site_id)->where('name', $this->name)->where('action', $this->action)->get();
        $lastFocus = null;
        $intervals = [];

        foreach($events as $event) {
            switch($event->type) {
                case FormEvent::$types['focus']:
                    $lastFocus = $event;
                break;

                case FormEvent::$types['blur']:
                    if($lastFocus && $event->time > $lastFocus->time) {
                        $intervals[] = $event->time - $lastFocus->time;
                    }
                    $lastFocus = null;
                break;

                default:
                    // We don't consider other events at the moment
                break;
            }

        }

        if($seconds) {
            $intervals = array_map(function($value) {
                return $value / 1000;
            }, $intervals);
        }

        sort($intervals, SORT_NUMERIC);
        $count = count($intervals);

        return [
            'count'     => $count,
            'min'       => $count > 0 ? min($intervals) : 0,
            'max'       => $count > 0 ? max($intervals) : 0,
            'avg'       => $count > 0 ? array_sum($intervals) / $count : 0,
            'median'    => $count > 0 ? $intervals[(int)($count / 2)] : 0,
            'histogram' => $this->histogram($intervals)
        ];
    }

    public function histogram($intervals) {
        $count = count($intervals);
        if($count === 0) return array_fill(0, 10, 0);
        $k = (int)ceil(log($count, 2) + 1);     // Sturges' formula
        $min = min($intervals);
        $max = max($intervals);
        $width = ($max - $min) / $k;

        if($min === $max) return [$count];

        $result = array_fill(0, $k + 1, 0);

        foreach($intervals as $time) {
            $bin = (int)(($time - $min) / $width);
            $result[$bin] += 1;
        }

        return $result;
    }

}
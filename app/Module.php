<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Module extends Model {

    const MOUSE = 1;
    const FORM = 2; 
    
    public $timestamps = false;

}
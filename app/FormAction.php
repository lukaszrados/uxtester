<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use App\FormEvent;
use App\FormLabel;

class FormAction extends Model {
    
    public $timestamps  = false;
    protected $table    = 'form_actions';
    protected $fillable = ['site_id', 'action'];

    public function site() {
        return $this->belongsTo(Site::class);
    }

    public function getFormLabels() {
        return FormLabel::where('action', $this->action)->where('site_id', $this->site_id)->get();
    }

}
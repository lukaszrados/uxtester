<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    protected $fillable = ['email', 'password'];
    protected $hidden = ['password'];

	public function sites() {
		return $this->hasMany(Site::class);
	}



}

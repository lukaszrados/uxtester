<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use App\Breakpoint;
use App\Visit;
use App\FormAction;
use App\FormEvent;
use App\FormLabel;

class Site extends Model {

    protected $fillable = ['track_id', 'user_id', 'base_url'];


    public function points() {
        return $this->hasMany(Point::class);
    }

    public function scrolls() {
        return $this->hasMany(Scroll::class);
    }

    public function breakpoints() {
        return $this->hasMany(Breakpoint::class);
    }

    public function formActions() {
        return $this->hasMany(FormAction::class);
    }

    public function formLabels() {
        return $this->hasMany(FormLabel::class);
    }

    public function hasAllScreenshots($width) {
        $breakpoints = $this->breakpoints;
        $result = [];

        foreach($breakpoints as $breakpoint) {
            if($width >= $breakpoint->min_width && $width <= $breakpoint->max_width && !$breakpoint->has_image) {
                return false;
            }
        }

        return true;
    }

    public function breakpointsByWidth($width) {
        $breakpoints = $this->breakpoints;
        $result = [];

        foreach($breakpoints as $breakpoint) {
            if($width >= $breakpoint->min_width && $width <= $breakpoint->max_width && !$breakpoint->has_image) {
                $result[] = $breakpoint;
            }
        }

        return $result;
    }

    public function getPointsByType($type) {
        return Point::where('site_id', $this->id)->where('type', $type)->get();
    }

    public function pointsByResolution($resolution, $type) {
        if(!$resolution['min']) $resolution['min'] = 0;
        if(!$resolution['max']) $resolution['max'] = PHP_INT_MAX;

        $points = Point::where('site_id', $this->id)->where('type', $type)->whereBetween('width', [$resolution['min'], $resolution['max']])->get(['x', 'y', 'width']);

        foreach($points as $point) {
            $point->x = Point::countPositionByCenter($point->x, $point->width);
        }

        return $points;
    }

    public function scrollsByResolution($resolution) {
        if(!$resolution['min']) $resolution['min'] = 0;
        if(!$resolution['max']) $resolution['max'] = PHP_INT_MAX;

        $scrolls = Scroll::where('site_id', $this->id)->whereBetween('width', [$resolution['min'], $resolution['max']])->get(['y', 'height']);

        return $scrolls;
    }


    /*
        Returns forms' statistics binded to current site object.
        Counts min, max, average and median time for each field
        Counts number of trials per visit fot each field
        Creates histogram for each field

        result:
        [{
            action:     FormAction object
            labels:     [
                0 => {
                    label:      FormLabel object,
                    min:        min time (float)
                    max:        max time (float)
                    avg:        average time (float)
                    median:     median time (float)
                    histogram:  array 
                }
            ]
        }, { action, labels }, {...} ]
    */

    public function formStatistics() {
        $result = [];
        $actions = $this->formActions;

        foreach($actions as $action) {

            $formLabels = $action->getFormLabels();
            $labels = [];

            foreach($formLabels as $label) {
                $labels[] = [
                    'label'         => $label,
                    'statistics'    => $label->statistics()
                ];
            }

            $result[] = [
                'action'    => $action,
                'labels'    => $labels
            ];
        }

        return $result;
    }


}
var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;

elixir(function(mix) {

    mix.sass('app.scss');
    mix.scripts(['Common/coder.js', 'Common/ajax.js', 'MouseTracker/main.js'], 'public/js/MouseTracker.js');
    mix.scripts(['Common/coder.js', 'Common/ajax.js', 'FormTracker/main.js'], 'public/js/FormTracker.js');
    mix.scripts(['Common/ajax.js', 'Visualization/click.js'], 'public/js/VisualizationClick.js');
    mix.scripts(['Common/ajax.js', 'Visualization/form.js'], 'public/js/VisualizationForm.js');
    // mix.version(['css/app.css', 'js/app.js']);

});

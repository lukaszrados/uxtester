<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Module;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('ModuleTableSeeder');
	}

}


class ModuleTableSeeder extends Seeder {

    public function run()
    {
        Module::create(['id' => 1, 'name' => 'MouseTracker']);
        Module::create(['id' => 2, 'name' => 'FormTracker']);
    }

}
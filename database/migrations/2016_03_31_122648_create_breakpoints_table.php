<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreakpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('breakpoints', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id');
			$table->integer('min_width');
			$table->integer('max_width');
			$table->boolean('has_image')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('breakpoints');
	}

}

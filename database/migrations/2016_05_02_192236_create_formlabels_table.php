<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormlabelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_labels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id');
			$table->string('name', 128);
			$table->string('action', 256);
			$table->string('label', 256);
			$table->integer('accuracy', 1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_labels');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormeventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id');

			$table->string('name', 64);
			$table->string('action', 256);
			$table->integer('type');
			$table->integer('time');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_events');
	}

}

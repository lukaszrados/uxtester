# UXTester

## Overview

This web application is the main part of my MA thesis entitled: "Analytical design of user's interfaces". Its main goal is allowing webpages' owners to track and analyse users' behaviour. During visit, various data is collected (like mouse movements, mouse clicks, scrolls and time required to fill each field of forms).

I would like to provide two ways of using this app: with or without using own database and/or PHP server. First option (with using own database) allows developers to download source code and install whole app on own server. This is the best option for advanced users. Less experienced pages' owners can (in the future) create account and generate key (unique for each site). All they need to do next is to copy and paste generated code into their HTML code (best - at the end of <body> section). Collected data is sent to my server and stored in database (of course user can access them after signing in).  

## Instaln

### Prerequirements

- PHP >= 5.5.*
- MySql >= 5.5
- [Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/en/)

### Installing Laravel and dependencies

Clone repositorium from bitbucket (or download package as zip and unzip it in your favourite location) and enter it's directory.
First, you need to ask composer to install all dependencies required by project.

```sh
$ composer install
```

After successfull installation rename .env.example file to .env and fill configuration (especially) database info.

### Setting up database

First create database with name you used in .env file. Next run migrations and seeding:

```sh
$ php artisan migrate
$ php artisan db:seed
```

### Running server

** Remember: artisan serve should NOT be used as development server **

```
$ php artisan serve
```

### Compiling JS and SCSS

All JS and SCSS files are already compiled for you in /public directory. If you wish to modify them then compilation is required:

```sh
$ gulp  // for single compilation
$ gulp watch // for compilation each time any JS/SCSS file is changed
```
